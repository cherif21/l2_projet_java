package ulco.cardGame.server;

import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.players.PokerPlayer;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Server Game management with use of Singleton instance creation
 */
public class SocketServer {

    private ServerSocket server;
    protected Game game; // game will be instantiate later
    protected Map<Player, Socket> playerSockets;
    protected Constructor playerConstructor;

    public static final int PORT = 7777;

    private SocketServer() {
        try {
            server = new ServerSocket(PORT);
            playerSockets = new HashMap<>();
            playerConstructor = null;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SocketServer server = new SocketServer();
        server.run();
    }

    private void run() {

        try {
            String gameChoix;
            Scanner scanner = new Scanner(System.in);
            do{
                System.out.println("Which game do you to play? Card OR Poker");
                gameChoix=scanner.nextLine();
            }while(!gameChoix.equals("Poker") && !gameChoix.equals("Card"));
            // Need to specify your current cardGame.txt file Or pokerGame.txt file
            if(gameChoix.equals("Card"))game = new CardGame("Battle", 3, "resources/games/cardGame.txt");
            else game=new PokerGame("Poker",3,2,"resources/games/pokerGame.txt");
            // Game Loop
            System.out.println("Waiting for new player for " + game.toString());

            // add each player until game is ready (or number of max expected player is reached)
            // Waiting for the socket entrance
            do {

                Socket socket = server.accept();

                // read socket Data
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                String playerUsername = (String) ois.readObject();

                // Create player instance
                Player player ;
                if(gameChoix.equals("Card")) player = new CardPlayer(playerUsername);
                else player = new PokerPlayer(playerUsername);
                // TODO : Add player in game
                if(game.addPlayer(socket,player)){
                    // Store player's socket in dictionary (Map)
                    playerSockets.put(player, socket);
                    System.out.println(player.getName() + " is now connected...");
                }

                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject(game.toString());

                // Tell to other players that new player is connected
                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("Now connected: " + player.getName());
                }

            } while (!game.isStarted());

            // run the whole game using sockets
            // TODO : Player gameWinner = game.run(playerSockets);

            Player gameWinner = game.run(playerSockets);
            //Envoyer un message a tous les joueurs le dernier statistique du jeu et  les infos du gagnant du jeu
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("the Winnner in the Game is "+gameWinner.toString());
            }
            // Tells to player that server will be closed (just as example)
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("END");
            }

            // Close each socket when game is finished
            for (Socket socket : playerSockets.values()) {
                socket.close();
            }

            game.removePlayers();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

