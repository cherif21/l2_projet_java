package ulco.cardGame.common.games.components;

public class Coin extends Component{
    /**
     * constructeur de la classe Coin
     * @param name
     * @param value
     */
    public Coin(String name, Integer value) {
        super(name, value);
    }
}
