package ulco.cardGame.common.games.components;

public class Card extends Component{
    /**
     * attribut hidden de la classe
     */
    private boolean hidden;

    /**
     * constructeur de la classe Card
     * @param name
     * @param valeur
     * @param hidden
     */
    public Card(String name,Integer valeur,boolean hidden){
        super(name,valeur);
        this.hidden=hidden;
    }

    /**
     * methode qui retourne l'etat de la carte
     * @return
     */
    public boolean isHidden(){
        return this.hidden;
    }

    /**
     * methode qui modifie l'etat de la carte
     * @param hidden
     */
    public void setHidden(boolean hidden){
        this.hidden=hidden;
    }

    /**
     * methode qui retourne les infos de la carte
     * @return
     */
    public String  toString(){
        return String.format("%s {Name=%s   Value=%d}",this.getClass().getSimpleName(),this.getName(),this.getValue());
    }
}
