package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public  class CardGame extends BoardGame{
    /**
     * attributs de la classe
     */
    private List<Card> cards;
    private Integer numberOfRounds=0;

    private String filename;
    /**
     * constructeur de la classe CardGame
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public CardGame(String name,Integer maxPlayers,String filename){
        super(name,maxPlayers,filename);
        this.filename=filename;
    }

    /**
     * Initialize the whole game using a parameter file
     * @param filename
     */
    public void initialize(String filename){
        List<Card> DataCards=new ArrayList<>();
        try{
            File cardFile=new File(filename);
            Scanner myReader=new Scanner(cardFile);
            while (myReader.hasNextLine()) {
                String data=myReader.nextLine();
                String name="";
                int value=0;
                //parcourir la les caracteres de la ligne en vue de recuperer le nom et la valeur de la carte
                for(int i=0;i<data.length();i++){
                    if(data.charAt(i)==';'){
                        String val="";
                        for(int j=i+1;j<data.length();j++){
                            val+=data.charAt(j);
                        }
                        value=Integer.parseInt(val);//convertir la valeur de la carte en entier
                        break;
                    }
                    name+=data.charAt(i);
                }
                Card card=new Card(name,value,false);
                DataCards.add(card);//ajouter la carte
            }
            myReader.close();
        }catch (FileNotFoundException e){
            System.out.println("Erreur de lecture");
            e.printStackTrace();
        }
        this.cards=DataCards;//mettre la liste des cartes dans l'attribut de la liste de cartes

    }



    /**
     * methode associé au boucle de jeu
     * @return
     */
         /*public Player run(Map<Player, Socket>playersSocket){

        //Melanger les cartes du jeu aleatoirement
        Collections.shuffle(this.cards);
        //Distribution des cartes a chacune des joueurs
        int indice=0;
        for(Card c:this.cards){
            this.getPlayers().get(indice).addComponent(c);//donner la carte au joueur
            indice+=1;//incrementer l'indice pour passer a un autre joueur
            if(indice==this.getPlayers().size()) indice=0;//recommencer quand de distribuer les cartes aux joueurs et que il ya de cartes
        }
        List<Player> gagnant=new ArrayList<>();//creation d'une liste de joueurs de gagant
        this.displayState();//afficher les info des jouers avant de retourner le gagant
        //affrontement des joueurs
        while(!this.end()){///tanque il n'y a pas un vainqueur on continue de jouer
            this.numberOfRounds+=1;//incremenatation du round
            System.out.println("ROUND "+this.numberOfRounds);//afficher le compteur de round
            Map<Player,Card>playedCard=new HashMap<>();//creation d'un dictionnaire permettant de memoriser le jouer et la carte qu'il a joue
            for(Player py:this.getPlayers()){//parcourir les joueurs pour jouer
                if(py.isPlaying()){//verifier si un joueur peut jouer
                    Card cd=(Card)py.play();//le joueur joue la carte et on le recupere
                    playedCard.put(py,cd);//ajoueter le joueur et sa carte dans le dictionnaire
                    System.out.printf("%s has played %s\n",py.getName(),cd.getName());//afficher un message pour dire aux autres joueurs qu'il a joue en donnant son nom et la carte joue
                }
            }

            //verifier quel joueur doit remporter le round

            int MaxCard=0;//variable permettant de sauvegarder la  plus haute carte
            for(Map.Entry<Player,Card>entry:playedCard.entrySet()){//parcourir les cartes jouees
                Player player=entry.getKey();//recuperer le joueur
                Card card=entry.getValue();//recuperer sa carte
                if(card.getValue()>=MaxCard){//verifier est ce la plus haute carte
                    if(MaxCard==card.getValue()) gagnant.add(player);//verifier s'il est egal a la plus haute carte pour l'ajouter a la liste de gagnant
                    else{
                        MaxCard=card.getValue();//sauvegarder la plus haute carte car elle a changé
                        if(!gagnant.isEmpty())gagnant.clear();//supprimer les joueurs si c'est pas vide parce que c'est pas eux qui possedent la plus haute carte
                        gagnant.add(player);//ajouter le jouer a la liste gagnant
                    }

                }
            }

            //ramasser les cartes gagne par le vainqueur de round
            if(gagnant.size()==1) {//verifier si on a un seul gagnant
                for(Map.Entry<Player,Card>entry:playedCard.entrySet()){//parcourir les cartes pour les ramasser
                    gagnant.get(0).addComponent(entry.getValue());//ajouter la carte a la liste de la carte du jouer qui a gagné
                    //modifier le statut du joueur s'il n'a plus de carte
                    if(entry.getKey().getComponents().isEmpty()) entry.getKey().canPlay(false);
                }
            }else{//si ya plusieurs jouers gagnant ils doivent aller en Batille jusqu'a ce que un joueur gagant
                System.out.println("Egalite entre joueurs");
                /*Aller en Battle *Decommenter la ligne pour tester*
                Player winBattle=new BattleGame(this.name,this.maxNumberOfPlayers(),this.filename).run(gagnant);
                for(Map.Entry<Player,Card>entry:playedCard.entrySet()){
                    winBattle.addComponent(entry.getValue());
                    //modifier le statut du joueur s'il n'a plus de carte
                    if(entry.getKey().getComponents().size()==0) entry.getKey().canPlay(false);
                }
                Fin batllle*//*
                //generer un gangant car ya plusieurs gagnant
                int aleaGagnant=new Random().nextInt(gagnant.size());
                //ramasser les cartes gagne par le vainqueur de round
                for(Map.Entry<Player,Card>entry:playedCard.entrySet()){
                    gagnant.get(aleaGagnant).addComponent(entry.getValue());
                    //modifier le statut du joueur s'il n'a plus de carte
                    if(entry.getKey().getComponents().size()==0) entry.getKey().canPlay(false);
                }

            }
            this.displayState();//afficher les info des jouers avant de retourner le gagant

            //melanger les cartes des joueurs apres chaque 10 rounds
            if(this.numberOfRounds%10==0){//verifier si on est au 10eme round
                for(Player py:this.getPlayers()){//parcourir les joueurs
                    py.shuffleHand();//melanger les cartes du joueur en main
                }
            }

        }
        this.endGame=true;//mettre fin au jeu
        return gagnant.get(0);//reutrn le gagant du joueur

    }*/

    /**
     * methode permetant de verifier si l'on est dans un état de fin de jeu: s'il a un gangnant
     * @return
     */
    public boolean end(){
      for(Player p:this.players){
          if(p.getScore()==this.cards.size()) return true;
      }
      return false;
    }

    /**
     * methode qui affichent les informations relatives au jeu
     * @return
     */
    public String toString(){
        return this.getClass().getSimpleName()+"{name='"+this.name+"'}";
    }

    /**
     * methode associé au boucle de jeu
     * @return
     */
    public Player run(Map<Player, Socket>playersSocket) throws IOException, ClassNotFoundException {
        //Melanger les cartes du jeu aleatoirement
        Collections.shuffle(this.cards);
        //Distribution des cartes a chacune des joueurs
        int indice=0;
        for(Card c:this.cards){
            this.getPlayers().get(indice).addComponent(c);//donner la carte au joueur
            indice+=1;//incrementer l'indice pour passer a un autre joueur
            if(indice==this.getPlayers().size()) indice=0;//recommencer quand de distribuer les cartes aux joueurs et que il ya de cartes
        }
        //Envoyer un message a tous les jouuers  le stat du game
        for(Socket socket:playersSocket.values()){
            ObjectOutputStream oos=new ObjectOutputStream (socket.getOutputStream());
            oos.writeObject((Game)this);
        }
        while(!this.end()) {///tanque il n'y a pas un vainqueur on continue de jouer
            this.numberOfRounds+=1;//incremenatation du round
            List<Player> gagnant=new ArrayList<>();//creation d'une liste de joueurs de gagant
            Map<Player,Card>playedCard=new HashMap<>();//creation d'un dictionnaire permettant de memoriser le jouer et la carte qu'il a joue
             for(Map.Entry<Player,Socket>ps: playersSocket.entrySet()){//parcourir les joueurs et leurs socket de jeu
                 //recuper le joueur et son socket
                 Player player=ps.getKey();
                 Socket socket=ps.getValue();
                 if(player.isPlaying()){//verifier s'il peut jouer
                     //Envoyer un message a tous les autres joueurs d'attendre que le jouer en cours joue sa carte
                     for(Socket socketPlayer:playersSocket.values()){
                         if(socketPlayer.equals(socket)) continue;
                         ObjectOutputStream oosPlayString=new ObjectOutputStream(socketPlayer.getOutputStream());
                         oosPlayString.writeObject("Waiting for "+player.getName()+" to play...");
                     }
                     //Envoyer un message au jouer courant de jouer
                     Map<String,CardGame>playUser=new Hashtable<>();
                     playUser.put("["+player.getName()+"] you have to play",this);
                     ObjectOutputStream oosPlayString=new ObjectOutputStream(socket.getOutputStream());
                     oosPlayString.writeObject(playUser);
                     //recuperer le composant carte qu'il a joue
                     ObjectInputStream oisCard=new ObjectInputStream(socket.getInputStream());
                     Card cardUser=(Card)oisCard.readObject();
                     //ajouter le joueur et sa carte dans le map
                     playedCard.put(player,cardUser);
                     //mettre la carte dans le CardBoard
                     this.getBoard().addComponent((Card)cardUser);
                     //supprimer la carte jouer de sa liste de carte
                     player.removeComponent(cardUser);
                     //Envoyer un message a tous les autres la carte que le joueur avec son nom
                     for(Socket socketPlayer:playersSocket.values()){
                         if(socketPlayer.equals(socket)) continue;
                         ObjectOutputStream oosPlayStringCard=new ObjectOutputStream(socketPlayer.getOutputStream());
                         oosPlayStringCard.writeObject("Player "+player.getName()+" has played "+cardUser.getName());
                     }
                 }

             }
             //preparer le messaage de l'etat du round
             String msgRound="-----------Board State-------------\n";
            for(Map.Entry<Player,Card>pc: playedCard.entrySet()){
                msgRound+=String.format("%s played by %s\n",pc.getKey().getName(),pc.getValue().getName());
            }
            msgRound+="-----------------------------------\n";
            //Envoyer le mesage de l'etat de round a tous les joueurs
            for(Socket socketPlayer:playersSocket.values()){
                ObjectOutputStream oosMsgRound=new ObjectOutputStream(socketPlayer.getOutputStream());
                oosMsgRound.writeObject(msgRound);
            }

            //verifier quel joueur doit remporter le round
            int MaxCard=0;//variable permettant de sauvegarder la  plus haute carte
            Card cardHaute=null;
            for(Map.Entry<Player,Card>entry:playedCard.entrySet()){//parcourir les cartes jouees
                Player player=entry.getKey();//recuperer le joueur
                Card card=entry.getValue();//recuperer sa carte
                if(card.getValue()>=MaxCard){//verifier est ce la plus haute carte
                    if(MaxCard==card.getValue()) gagnant.add(player);//verifier s'il est egal a la plus haute carte pour l'ajouter a la liste de gagnant
                    else{
                        MaxCard=card.getValue();//sauvegarder la plus haute carte car elle a changé
                        cardHaute=card;
                        if(!gagnant.isEmpty())gagnant.clear();//supprimer les joueurs si c'est pas vide parce que c'est pas eux qui possedent la plus haute carte
                        gagnant.add(player);//ajouter le jouer a la liste gagnant
                    }

                }
            }

        //ramasser les cartes du jeu se trouvant sur le plateau CardBoard
        int placeGagnant=(gagnant.size()==1) ? 0:new Random().nextInt(gagnant.size());
            for(Component comp:this.getBoard().getSpecificComponents(Card.class)){
                gagnant.get(placeGagnant).addComponent(comp);
        }
        //Envoyer un message a tous les joueurs le gagnat du round
        String msgWinRound=String.format("Player :%s won the round width %s ",gagnant.get(placeGagnant).toString(),cardHaute.toString());
                for(Socket socketPlayer:playersSocket.values()){
            ObjectOutputStream oosMsgWinRound=new ObjectOutputStream(socketPlayer.getOutputStream());
            oosMsgWinRound.writeObject(msgWinRound);
        }

        //  nettoyer le jeu de plateau CardBoard
                this.getBoard().clear();
        //melanger les cartes des joueurs apres chaque 10 rounds et verifier s'il continue a jouer ou non
                if(this.numberOfRounds%10==0){//verifier si on est au 10eme round
            for(Player py:this.getPlayers()){//parcourir les joueurs
                if(py.getScore()==0) {
                    py.canPlay(false);
                    System.out.println("ne peut plus jouer");
                }
                if(py.isPlaying())py.shuffleHand();//melanger les cartes du joueur en main
            }
        }
    }

    //Envoyer le gagnant du jeu
            for(Player p:this.getPlayers()){
                if(p.getScore()==this.cards.size()) return p;
                    }
                    return null;
        }
}
