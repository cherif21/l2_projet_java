package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class CardBoard implements Board {
    private List<Card>cards=new ArrayList<Card>();
    /**
     * nettoyer le Board
     */
    public void clear() {
        this.cards.clear();
    }

    /**
     * ajouter un composant
     *
     * @param component
     */
    public void addComponent(Component component) {
        this.cards.add((Card)component);
    }

    /**
     * retourner une liste de composant
     *
     * @return
     */
    public List<Component> getComponents() {
        return new ArrayList<>(this.cards);
    }

    /**
     * retourner une liste de composant specific
     *
     * @param classType
     * @return
     */
    public List<Component> getSpecificComponents(Class classType) {
        return new ArrayList<>(this.cards);
    }

    /**
     * afficher les informations de Board
     */
    public void displayState() {
        System.out.println("-----------");
        for(Card c:this.cards){
            System.out.println("Card: "+c.getName());
        }
        System.out.println("-----------");
    }
}
