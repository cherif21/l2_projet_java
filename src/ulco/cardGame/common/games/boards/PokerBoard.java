package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class PokerBoard implements Board {
    private List<Card>cards=new ArrayList<>();
    private List<Coin>coins=new ArrayList<>();
    /**
     * nettoyer le Board
     */
    public void clear() {
        this.cards.clear();
        this.coins.clear();
    }

    /**
     * ajouter un composant
     *
     * @param component
     */
    public void addComponent(Component component) {
        if(component instanceof Coin) this.coins.add((Coin)component);
        else this.cards.add((Card)component);
    }

    /**
     * retourner une liste de composant
     *
     * @return
     */
    public List<Component> getComponents() {
        ArrayList<Component>comp=new ArrayList<>();
        comp.addAll(this.cards);
        comp.addAll(this.coins);
        return comp;
    }

    /**
     * retourner une liste de composant specific
     *
     * @param classType
     * @return
     */
    public List<Component> getSpecificComponents(Class classType) {
        if(classType==Card.class) return new ArrayList<>(this.cards);
        return new ArrayList<>(this.coins);
    }

    /**
     * afficher les informations de PokerBoard
     */
    public void displayState() {
        System.out.println("----------------------------------------------------------------");
        for(Card ca:this.cards) System.out.println("Card: "+ca.getName());
        System.out.println("---------------");
        for(Coin co:this.coins) System.out.println("Coin: "+co.getName()+" x "+co.getValue());
        System.out.println("----------------------------------------------------------------");
    }
}
