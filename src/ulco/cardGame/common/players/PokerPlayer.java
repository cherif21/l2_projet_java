package ulco.cardGame.common.players;

import ulco.cardGame.common.games.BoardPlayer;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PokerPlayer extends BoardPlayer {
    private List<Card>cards=new ArrayList<>();
    private List<Coin> coins=new ArrayList<>();

    /**
     * constructeur de la classe PokerPlayer
     * @param name
     */
    public PokerPlayer(String name){
        super(name);
    }
    /**
     * Player do an action into a Game
     */
    /*public Component play() {
        String nameJeton="";
        do{
            System.out.println("please select a valid Coin to play (coin color) OR Entry the Word (noMise) if you don't mise");
            nameJeton=new Scanner(System.in).next();//saisir le nom du jeton(couleur)
            if(nameJeton.equals("noMise")) return null;//s'il entre noMise il ne veut pas jouer son tour alors il doit plus continuer la manche
        }while(!nameJeton.equals("Red") && !nameJeton.equals("Blue") && !nameJeton.equals("Black"));
        for(Coin co:this.coins) {//parcourir les jetons du joueurs
            if (co.getName().equals(nameJeton)) {//verifier si c'est  ce jeton qu'il veut
                Coin coin = co;//stocker le jeton qu'il veut
                this.removeComponent(co);//supprimer le jeton
                return coin;//retourner le jeton
            }
        }
        return null;//retourner null s'il ne trouve pas le jeton
    }*/

    /**
     * methode qui retourne le score du joueur
     * @return
     */
    public Integer getScore(){
        return this.score;
    }

    /**
     * Player do an action into a Game
     *
     * @param socket
     */
    public void play(Socket socket) throws IOException {
        String nameJeton="";
        boolean exist=false;
        do{
            do{
                System.out.println("please select a valid Coin to play (coin color) OR Entry the Word (noMise) if you don't mise");
                nameJeton=new Scanner(System.in).nextLine();//saisir lequalse nom du jeton(couleur)
                if(nameJeton.equals("noMise"))  break;
            }while(!nameJeton.equals("Red") && !nameJeton.equals("Blue") && !nameJeton.equals("Black"));
            if(nameJeton.equals("noMise")){
                ObjectOutputStream oosCoin=new ObjectOutputStream(socket.getOutputStream());
                oosCoin.writeObject(null);
                exist=true;
            }else{
                for(Coin co:this.coins) {//parcourir les jetons du joueurs
                    if (co.getName().equals(nameJeton)) {//verifier si c'est  ce jeton qu'il veut
                        ObjectOutputStream oosCoin=new ObjectOutputStream(socket.getOutputStream());
                        oosCoin.writeObject(co);
                        exist=true;
                        break;

                    }
                }
            }

           if(!exist) System.out.println(nameJeton+"est fini de vos liste de jeton saisissez un autre");
        }while(!exist);

    }

    /**
     * Add new component linked to Player
     *
     * @param component
     */
    public void addComponent(Component component) {
        if(component instanceof Coin) {
            this.coins.add((Coin)component);
            this.score+=component.getValue();
        }
        else this.cards.add((Card)component);
    }

    /**
     * Remove component from Player hand
     *
     * @param component
     */
    public void removeComponent(Component component) {
        if(component instanceof Coin){
            this.coins.remove(component);
            this.score-=component.getValue();
        }
        else this.cards.remove(component);
    }

    /**
     * Get all component in Player hand
     *
     * @return list of components
     */
    public List<Component> getComponents() {
        ArrayList<Component>comp=new ArrayList<>();
        comp.addAll(this.cards);
        comp.addAll(this.coins);
        return comp;
    }

    /**
     * retourner une liste de composant specific
     *
     * @param classType
     * @return
     */
    public List<Component> getSpecificComponents(Class classType) {
        if(classType==Card.class) return new ArrayList<>(this.cards);
        return new ArrayList<>(this.coins);
    }

    /**
     * Shuffle components hand of current Player
     */
    public void shuffleHand() {
        Collections.shuffle(this.cards);
    }

    /**
     * Remove all components from Player hand
     */
    public void clearHand() {
        this.cards.clear();
        //this.coins.clear();
    }
    /**
     * afficher les informations des composant de la main du joueur
     */
    public void displayHand() {
        System.out.println("----------------------------------------------------------------");
        System.out.printf("Hand of [%s]\n",this.getName());
        System.out.println("\t---------------");
        int coinRed=0,coinBlack=0,coinBlue=0;
        for(Card ca:this.cards) System.out.println("Card: "+ca.getName());
        System.out.println("\t---------------");
        for(Coin co:this.coins) {
           switch (co.getName()){
               case "Red":
                   coinRed+=1;
                   break;
               case "Black":
                   coinBlack+=1;
                   break;
               case "Blue":
                   coinBlue+=1;
                   break;
           }
        }
        if(coinRed>0)System.out.println("-Coin Red x "+coinRed);
        if(coinBlue>0)System.out.println("-Coin Blue x "+coinBlue);
        if(coinBlack>0)System.out.println("-Coin Red x "+coinBlue);
        System.out.println("Your Coins sum is about:"+this.getScore());
        System.out.println("----------------------------------------------------------------");
    }
    /**
     * methode qui affiche les informations du joueur
     * @return
     */
    public String toString(){
        return  super.toString();
    }
}
